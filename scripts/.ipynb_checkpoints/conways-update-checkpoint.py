

# update Funktion, die den nächsten Schritt des Automaten berechnet
def update(data):
    global B

    # kopie des Rasters vom letzten Schritt erstellen
    newGrid=B.copy()

    # Anzahl der Nachbarn für jede Zelle berechnen und in das Hilfsarray "nbrs_count" schreiben	
    nbrs_count = convolve2d(newGrid, kernel, mode='same', boundary='wrap')


    # Regel von Conway, Zellen die weniger als 2 und mehr als 3 Nachbarn haben rausfinden
    # und an diesen Stellen das Raster von 1 auf 0 setzen (d....dead)
    d = (nbrs_count < 2)| (nbrs_count >3)
    newGrid[d]=False

    # Regel von Conway, Zellen die genau 3 Nachbarn haben rausfinden
    # und an diesen Stellen das Raster von 0 auf 1 setzten (n....newborn)
    n= (nbrs_count == 3 )
    newGrid[n]=True

    # neue Werte im Raster abspeichern
    mat.set_data(newGrid)
    
    # altes Raster mit neuem Raster überschreiben
    B = newGrid
    return [mat]
