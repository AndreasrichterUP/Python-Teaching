


def CalcNetAdjustmentStep(data,i):
    
    import numpy as np
    
    # calculated observations 
    data[21,2,i]=np.sqrt((data[2,2,i]-data[6,2,i] )**2 + (data[2,3,i]-data[6,3,i])**2)
    data[22,2,i]=np.sqrt((data[4,2,i]-data[6,2,i] )**2 + (data[4,3,i]-data[6,3,i])**2)
    data[23,2,i]=np.sqrt((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[24,2,i]=np.sqrt((data[2,2,i]-data[7,2,i] )**2 + (data[2,3,i]-data[7,3,i])**2)
    data[25,2,i]=np.sqrt((data[4,2,i]-data[7,2,i] )**2 + (data[4,3,i]-data[7,3,i])**2)
    data[26,2,i]=np.sqrt((data[6,2,i]-data[7,2,i] )**2 + (data[6,3,i]-data[7,3,i])**2)
  
   
    data[27,2,i] = np.arctan2((data[2,3,i]-data[6,3,i]),(data[2,2,i]-data[6,2,i] ))
    data[28,2,i] = np.arctan2((data[3,3,i]-data[6,3,i]),(data[3,2,i]-data[6,2,i] ))
    data[29,2,i] = np.arctan2((data[4,3,i]-data[6,3,i]),(data[4,2,i]-data[6,2,i] ))
    data[30,2,i] = np.arctan2((data[7,3,i]-data[6,3,i]),(data[7,2,i]-data[6,2,i] ))
    
    data[31,2,i] = np.arctan2((data[1,3,i]-data[7,3,i]),(data[1,2,i]-data[7,2,i] ))
    data[32,2,i] = np.arctan2((data[2,3,i]-data[7,3,i]),(data[2,2,i]-data[7,2,i] ))+2*np.pi
    data[33,2,i] = np.arctan2((data[4,3,i]-data[7,3,i]),(data[4,2,i]-data[7,2,i] ))+2*np.pi
    data[34,2,i] = np.arctan2((data[5,3,i]-data[7,3,i]),(data[5,2,i]-data[7,2,i] ))
    data[35,2,i] = np.arctan2((data[6,3,i]-data[7,3,i]),(data[6,2,i]-data[7,2,i] ))+2*np.pi

    # Delta L Vector
    data[21:36,3,i]=data[21:36,1,i] - data[21:36,2,i]
    
    
    # Create P Matrix
    for index in range(9):
        if index < 6:
            data[1+index, 5+index,i] = ((np.pi*10**-5)**2)/(0.02**2)
        data[7+index,11+index,i] = 1
    
    
    
    # calc Design Matrix
    data[21,5,i] = (data[6,2,i]-data[2,2,i] )/np.sqrt((data[2,2,i]-data[6,2,i] )**2 + (data[2,3,i]-data[6,3,i])**2)
    data[21,6,i] = (data[6,3,i]-data[2,3,i] )/np.sqrt((data[2,2,i]-data[6,2,i] )**2 + (data[2,3,i]-data[6,3,i])**2)
    data[21,7:11,i] = 0
    
    data[22,5,i] = (data[6,2,i]-data[4,2,i] )/np.sqrt((data[4,2,i]-data[6,2,i] )**2 + (data[4,3,i]-data[6,3,i])**2)
    data[22,6,i] = (data[6,3,i]-data[4,3,i] )/np.sqrt((data[4,2,i]-data[6,2,i] )**2 + (data[4,3,i]-data[6,3,i])**2)

    
    data[23,5,i] = (data[6,2,i]-data[7,2,i] )/np.sqrt((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[23,6,i] = (data[6,3,i]-data[7,3,i] )/np.sqrt((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[23,7,i] = (data[7,2,i]-data[6,2,i] )/np.sqrt((data[6,2,i]-data[7,2,i] )**2 + (data[6,3,i]-data[7,3,i])**2)
    data[23,8,i] = (data[7,3,i]-data[6,3,i] )/np.sqrt((data[6,2,i]-data[7,2,i] )**2 + (data[6,3,i]-data[7,3,i])**2)

    data[24,7,i] = (data[7,2,i]-data[2,2,i] )/np.sqrt((data[7,2,i]-data[2,2,i] )**2 + (data[7,3,i]-data[2,3,i])**2)
    data[24,8,i] = (data[7,3,i]-data[2,3,i] )/np.sqrt((data[7,2,i]-data[2,2,i] )**2 + (data[7,3,i]-data[2,3,i])**2)

    data[25,7,i] = (data[7,2,i]-data[4,2,i] )/np.sqrt((data[7,2,i]-data[4,2,i] )**2 + (data[7,3,i]-data[4,3,i])**2)
    data[25,8,i] = (data[7,3,i]-data[4,3,i] )/np.sqrt((data[7,2,i]-data[4,2,i] )**2 + (data[7,3,i]-data[4,3,i])**2)

    data[26,5,i] = (data[6,2,i]-data[7,2,i] )/np.sqrt((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[26,6,i] = (data[6,3,i]-data[7,3,i] )/np.sqrt((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[26,7,i] = -(data[6,2,i]-data[7,2,i] )/np.sqrt((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[26,8,i] = -(data[6,3,i]-data[7,3,i] )/np.sqrt((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)

    
    data[27,5,i] =  (data[2,3,i]-data[6,3,i] )/((data[2,2,i]-data[6,2,i] )**2 + (data[2,3,i]-data[6,3,i])**2)
    data[27,6,i] = -(data[2,2,i]-data[6,2,i] )/((data[2,2,i]-data[6,2,i] )**2 + (data[2,3,i]-data[6,3,i])**2)
    #data[27,9,i] = 1.0
     
    data[28,5,i] =  (data[3,3,i]-data[6,3,i] )/((data[3,2,i]-data[6,2,i] )**2 + (data[3,3,i]-data[6,3,i])**2)
    data[28,6,i] = -(data[3,2,i]-data[6,2,i] )/((data[3,2,i]-data[6,2,i] )**2 + (data[3,3,i]-data[6,3,i])**2)
    #data[28,9,i] = 1.0

    data[29,5,i] =  (data[4,3,i]-data[6,3,i] )/((data[4,2,i]-data[6,2,i] )**2 + (data[4,3,i]-data[6,3,i])**2)
    data[29,6,i] = -(data[4,2,i]-data[6,2,i] )/((data[4,2,i]-data[6,2,i] )**2 + (data[4,3,i]-data[6,3,i])**2)
    #data[29,9,i] = 1.0
    
    data[30,5,i] =  (data[7,3,i]-data[6,3,i] )/((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[30,6,i] = -(data[7,2,i]-data[6,2,i] )/((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[30,7,i] = -(data[7,3,i]-data[6,3,i] )/((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[30,8,i] =  (data[7,2,i]-data[6,2,i] )/((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[27:31,9,i] = 1.0

    data[31,7,i] =  (data[1,3,i]-data[7,3,i] )/((data[1,2,i]-data[7,2,i] )**2 + (data[1,3,i]-data[7,3,i])**2)
    data[31,8,i] = -(data[1,2,i]-data[7,2,i] )/((data[1,2,i]-data[7,2,i] )**2 + (data[1,3,i]-data[7,3,i])**2)

    data[32,7,i] =  (data[2,3,i]-data[7,3,i] )/((data[2,2,i]-data[7,2,i] )**2 + (data[2,3,i]-data[7,3,i])**2)
    data[32,8,i] = -(data[2,2,i]-data[7,2,i] )/((data[2,2,i]-data[7,2,i] )**2 + (data[2,3,i]-data[7,3,i])**2)

    data[33,7,i] =  (data[4,3,i]-data[7,3,i] )/((data[4,2,i]-data[7,2,i] )**2 + (data[4,3,i]-data[7,3,i])**2)
    data[33,8,i] = -(data[4,2,i]-data[7,2,i] )/((data[4,2,i]-data[7,2,i] )**2 + (data[4,3,i]-data[7,3,i])**2)

    data[34,7,i] =  (data[5,3,i]-data[7,3,i] )/((data[5,2,i]-data[7,2,i] )**2 + (data[5,3,i]-data[7,3,i])**2)
    data[34,8,i] = -(data[5,2,i]-data[7,2,i] )/((data[5,2,i]-data[7,2,i] )**2 + (data[5,3,i]-data[7,3,i])**2)

    data[35,5,i] =  (data[7,3,i]-data[6,3,i] )/((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[35,6,i] = -(data[7,2,i]-data[6,2,i] )/((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[35,7,i] = -(data[7,3,i]-data[6,3,i] )/((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
    data[35,8,i] =  (data[7,2,i]-data[6,2,i] )/((data[7,2,i]-data[6,2,i] )**2 + (data[7,3,i]-data[6,3,i])**2)
 
    
    data[31:36,10,i] = 1.0


    
    #data[35,5,i]
    
    return data