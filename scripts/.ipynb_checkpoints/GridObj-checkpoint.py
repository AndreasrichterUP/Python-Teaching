# use powerfull predefined librarys to read georeferenced data, and georef information, cellsize etc
# make it flexible to read data with gdal or with rasterio (rasterio is preferred)
# use format geotif preferably
# also use 3 coordinate matrices X,Y,Elevation
# check for nodata values, do inputation e.g. max(int)
# check for same cellsize in x and y coordinates
# check for projected coordinate system



class GridObj():
    
    # Each Grid has these attributes.
    def __init__(self,filename):  
        import rasterio
        DEMdata=rasterio.open(filename)
        self.dem = DEMdata
        self.Z = DEMdata.read(1)
        self.cellsize = DEMdata.transform[0]
        self.refmat = DEMdata.transform
        self.size = DEMdata.shape
        self.name = filename
        self.zunit = 0
        self.xyunit = 0
        self.georef = 0
        
    
    #    self.Z = DEMdata.read(1)
    #    self.name = DEMdata.name
    #    self.refmat = DEMdata.transform
    #    self.cellsize = DEMdata.transform[0]
    #    self.size = DEMdata.shape
        
    
    #def read(self, filename):
    #    import numpy as np
    #    import rasterio
    #    from osgeo import gdal,osr,ogr
        
    #    DEMdata=rasterio.open(filename)
        #print(DEMdata.name)
        #print(DEMdata.crs)
        #print(DEMdata.mode)
        #print(DEMdata.transform)
        #print(DEMdata.shape)
        
    #    self.Z = DEMdata.read(1)
    #    self.name = DEMdata.name
    #    self.refmat = DEMdata.transform
    #    self.cellsize = DEMdata.transform[0]
    #    self.size = DEMdata.shape
        
        
        
     #   return self
        

    
    
    
 
    
    



# https://phillipmfeldman.org/Python/Advantages_of_Python_Over_Matlab.html