class Veroeffentlichung:
    def __init__(self, titel, preis):
        self.titel = titel
        self.preis = preis

class Zeitschrift(Veroeffentlichung):
    def __init__(self, titel, verlag, periode, preis ):
        super().__init__(titel, preis)
        self.periode = periode
        self.verlag = verlag


class Buch(Veroeffentlichung):
    def __init__(self, titel, autor, seitenzahl, preis):
        super().__init__(titel, preis)
        self.seitenzahl = seitenzahl
        self.autor = autor
    
class Magazin(Zeitschrift):
    def __init__(self, titel, verlag, periode, preis):
        super().__init__(titel, verlag, periode, preis)
    
class Tageszeitung(Zeitschrift):
    def __init__(self, titel, verlag, periode, preis):
        super().__init__(titel, verlag, periode, preis)


#class Book:
#    def __init__(self, title, author, pages, price):
#        self.title = title
#        self.author = author
#        self.pages = pages
#        self.price = price
    
#class Magazine:
#    def __init__(self, title, publisher, period, price):
#        self.name = title
#        self.publisher = publisher
#        self.period = period
#        self.price = price
    
#class Newspaper:
#    def __init__(self, title, publisher, period, price):
#        self.name = title
#        self.publisher = publisher
#        self.period = period
#        self.price = price



class Rocket():
    # Rocket simulates a rocket ship for a game,
    #  or a physics simulation.
    
    def __init__(self):
        # Each rocket has an (x,y) position.
        self.x = 0
        self.y = 0
        self.z = 0
    def move_up(self):
        # Increment the y-position of the rocket.
        self.y += 1
        
        
class Privat(object):
    
    def __init__(self):
        self.__ganzprivat = 1
        self._privat = 2
        self.oeffentlich = 3
        
class Ding(object):
    dichte = {"Gold":19.32, "Eisen":7.87, "Silber":10.5}
    def __init__(self, stoff, volumen):
        self.volumen = volumen
        self.stoff = stoff
        
    def masse(self):
        return self.dichte[self.stoff]*self.volumen
        
        
class M(object):
    
    def __init__(self,wert, einheit="m"):
        self.wert = wert
        self.einheit = einheit
        if einheit == "mm": self.mm = wert
        elif einheit == "cm": self.mm = wert*10
        elif einheit == "m": self.mm = wert*1000
    
    def __cmp__(self,other):
        if self.mm < other.mm: return -1
        elif self.mm == other.mm: return 0
        else: return 1
        
    def __add__(self,other):
        summe = float(self.mm) + float(other.mm)
        if summe > 1000:
            einheit = "m"
            summe /= 1000
        elif summe > 10:
            einheit = "cm"
            summe /= 10
        else: einheit = "mm"
        return M(summe, einheit)
    
    def __str__(self):
        return str(self.wert) + " " + self.einheit